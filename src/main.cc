#include <iostream>
#include <memory>

#include <boost/regex.hpp>

#include "SimpleTreeNode.h"
#include "TreeProblems.h"

using namespace std;
using namespace SimpleTree;
using namespace TreeProblems;

int main() {
    //  Just playing around in here.  Actual testing is done in the test classes.
    int bar = 31415;
    Value<int> value = make_shared<int>(bar);
    SimpleTreeNode<int> node(value);
    cout << "Node: " << node << endl;

    {
        int foo = 12345;
        shared_ptr<int> foo_ptr = make_shared<int>(foo);
        cout << *foo_ptr << endl;
        node.setValue(foo_ptr);
    }

    cout << "Node address: " << &node << endl;
    cout << "Node value: " << node << endl;

    cout << endl << "Some regex:" << endl;
    string line("O HAI!");
    boost::regex pat(".*HAI.*");

    boost::smatch matches;
    if (boost::regex_match(line, matches, pat))
        std::cout << matches << std::endl;

    return 0;
}

