#include "gtest/gtest.h"

#include <deque>
#include <memory>
#include <queue>
#include <tuple>

#include "TreeProblems.h"

using namespace std;
using namespace TreeProblems;

namespace TreeTraversalTests {

    /**
     * Builds a simple balanced tree of integer values that reads out in order when
     * traversed pre-order.
     */
    Node<int> buildSimpleTestTree(const int depth) {

        int nodeId = 0;
        const Node<int> head = make_node<int>(nodeId++);
        head->setHead(head);

        auto nodeQueue = deque<Node<int>>();
        nodeQueue.push_front(head);

        int depthCounter = 0;

        while (nodeQueue.empty() == false) {
            Node<int> current = nodeQueue.front();

            if (depthCounter < depth && current->getLeft() == nullptr) {
                Node<int> newNode = make_node<int>(nodeId++);
                newNode->setHead(head);
                current->setLeft(newNode);
                nodeQueue.push_front(newNode);
                depthCounter++;
            } else if (depthCounter < depth && current->getRight() == nullptr) {
                Node<int> newNode = make_node<int>(nodeId++);
                newNode->setHead(head);
                current->setRight(newNode);
                nodeQueue.push_front(newNode);
                depthCounter++;
            } else {
                nodeQueue.pop_front();
                depthCounter--;
            }
        }

        return head;
    }
    template<typename T>
    using TraversalMethod = void (*)(Node<T>, ostringstream &);

    template<typename T>
    struct SimpleTreeTestCase {
        TraversalMethod<T> testMethod;
        Node<T> headNode;
        string expectedResult;
    };

    template<typename T>
    using TreeTest = shared_ptr<SimpleTreeTestCase<T>>;

    template<typename T>
    TreeTest<T> make_test(TraversalMethod<T> testMethod, NodePtr<T> headNode, string expectedResult) {
        SimpleTreeTestCase<T> test { testMethod, make_shared<SimpleTreeNode<T>>(*headNode), expectedResult };
        return make_shared<SimpleTreeTestCase<T>>(test);
    }

    template<typename T>
    TreeTest<T> make_test(TraversalMethod<T> testMethod, Node<T> headNode, string expectedResult) {
        SimpleTreeTestCase<T> test { testMethod, headNode, expectedResult };
        return make_shared<SimpleTreeTestCase<T>>(test);
    }

    //  M,L,R
    vector<TreeTest<int>> preOrderTestCases {
        make_test(preOrderRecursive, buildSimpleTestTree(0), "0"),
        make_test(preOrderRecursive, buildSimpleTestTree(0)->setLeft(make_node(1)), "01"),
        make_test(preOrderRecursive, buildSimpleTestTree(0)->setRight(make_node(1)), "01"),
        make_test(preOrderRecursive, buildSimpleTestTree(1), "012"),
        make_test(preOrderRecursive, buildSimpleTestTree(1)->getLeft()->setLeft(make_node(3))->getHead(), "0132"),
        make_test(preOrderRecursive, buildSimpleTestTree(1)->getLeft()->setRight(make_node(3))->getHead(), "0132"),
        make_test(preOrderRecursive, buildSimpleTestTree(1)->getRight()->setLeft(make_node(3))->getHead(), "0123"),
        make_test(preOrderRecursive, buildSimpleTestTree(1)->getRight()->setRight(make_node(3))->getHead(), "0123"),
        make_test(preOrderRecursive, buildSimpleTestTree(2), "0123456"),
    };

    //  L,M,R
    vector<TreeTest<int>> inOrderTestCases {
        make_test(inOrderRecursive, buildSimpleTestTree(0), "0"),
        make_test(inOrderRecursive, buildSimpleTestTree(0)->setLeft(make_node(1)), "10"),
        make_test(inOrderRecursive, buildSimpleTestTree(0)->setRight(make_node(1)), "01"),
        make_test(inOrderRecursive, buildSimpleTestTree(1), "102"),
        make_test(inOrderRecursive, buildSimpleTestTree(1)->getLeft()->setLeft(make_node(3))->getHead(), "3102"),
        make_test(inOrderRecursive, buildSimpleTestTree(1)->getLeft()->setRight(make_node(3))->getHead(), "1302"),
        make_test(inOrderRecursive, buildSimpleTestTree(1)->getRight()->setLeft(make_node(3))->getHead(), "1032"),
        make_test(inOrderRecursive, buildSimpleTestTree(1)->getRight()->setRight(make_node(3))->getHead(), "1023"),
        make_test(inOrderRecursive, buildSimpleTestTree(2), "2130546"),
    };

    //  L,R,M
    vector<TreeTest<int>> postOrderTestCases {
        make_test(postOrderRecursive, buildSimpleTestTree(0), "0"),
        make_test(postOrderRecursive, buildSimpleTestTree(0)->setLeft(make_node(1)), "10"),
        make_test(postOrderRecursive, buildSimpleTestTree(0)->setRight(make_node(1)), "10"),
        make_test(postOrderRecursive, buildSimpleTestTree(1), "120"),
        make_test(postOrderRecursive, buildSimpleTestTree(1)->getLeft()->setLeft(make_node(3))->getHead(), "3120"),
        make_test(postOrderRecursive, buildSimpleTestTree(1)->getLeft()->setRight(make_node(3))->getHead(), "3120"),
        make_test(postOrderRecursive, buildSimpleTestTree(1)->getRight()->setLeft(make_node(3))->getHead(), "1320"),
        make_test(postOrderRecursive, buildSimpleTestTree(1)->getRight()->setRight(make_node(3))->getHead(), "1320"),
        make_test(postOrderRecursive, buildSimpleTestTree(2), "2315640"),
    };

    class SimpleTreeTraversalTests : public ::testing::TestWithParam<TreeTest<int>> { };

    INSTANTIATE_TEST_CASE_P(PreOrderTraversal,  SimpleTreeTraversalTests, ::testing::ValuesIn(preOrderTestCases));
    INSTANTIATE_TEST_CASE_P(InOrderTraversal,   SimpleTreeTraversalTests, ::testing::ValuesIn(inOrderTestCases));
    INSTANTIATE_TEST_CASE_P(PostOrderTraversal, SimpleTreeTraversalTests, ::testing::ValuesIn(postOrderTestCases));

    TEST_P(SimpleTreeTraversalTests, PreOrderTraversal) {
        auto testCase = GetParam();
        ostringstream result;
        testCase->testMethod(testCase->headNode, result);
        ASSERT_EQ(testCase->expectedResult, result.str());
    }

    TEST(TreeTraversals, TreeGeneratorFieldsAreCorrect) {
        const Node<int> head = buildSimpleTestTree(2);

        queue<Node<int>> nodeQueue;
        nodeQueue.push(head);

        while (nodeQueue.size() > 0) {
            Node<int> current = nodeQueue.front();
            nodeQueue.pop();

            EXPECT_EQ(head, current->getHead());

            if (current->getLeft()  != nullptr) nodeQueue.push(current->getLeft());
            if (current->getRight() != nullptr) nodeQueue.push(current->getRight());
        }
    }
}
