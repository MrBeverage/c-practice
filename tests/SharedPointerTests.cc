#include <memory>

#include "gtest/gtest.h"

using namespace std;

/**
 * Playing around with and trying to better understand the new smart pointers in C++ 11.
 */
namespace SharedPointerTests {

    struct TestStruct {
        shared_ptr<int> sharedPtr;
        int value;
    };

    TEST(SharedPointerTest, ValueExistsOutOfDeclarationScope) {
        TestStruct test;
        shared_ptr<TestStruct> structPtr = make_shared<TestStruct>(test);

        {
            shared_ptr<int> intPtr = make_shared<int>(12345);
            structPtr->sharedPtr = intPtr;
            structPtr->value = 12345;
        }

        EXPECT_EQ(12345, *(structPtr->sharedPtr));
        EXPECT_EQ(12345, structPtr->value);
    }

    TEST(SharedPointerTest, RefCountDecreasedWhenSecondPointerGoesOutOfScope) {
        TestStruct test;
        shared_ptr<TestStruct> structPtr = make_shared<TestStruct>(test);
        structPtr->sharedPtr = make_shared<int>(12345);

        {
            shared_ptr<int> secondPtr = structPtr->sharedPtr;
            EXPECT_EQ(2, structPtr->sharedPtr.use_count());
        }

        EXPECT_EQ(1, structPtr->sharedPtr.use_count());
    }
}
