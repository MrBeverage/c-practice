
#include <memory>

#include "gtest/gtest.h"

#include "macros.h"

using namespace std;

namespace MacrosTest {

    class MacroTestClass {
        PROPERTY(Property1, int)
        FLUENT_PROPERTY(Property3, int)
        FLUENT_PROPERTY_SHARED_PTR(Property4, int)
    };

    TEST(MacrosTest, NormalPropertySetAndGet) {
        MacroTestClass test;
        test.setProperty1(10);
        ASSERT_EQ(test.getProperty1(), 10);
    }

    TEST(MacrosTest, FluentPropertyReturnsThis) {
        MacroTestClass test;
        ASSERT_EQ(&test, test.setProperty3(10));
    }

    TEST(MacrosTest, FluentPropertySetAndGet) {
        MacroTestClass test;
        test.setProperty3(10);
        ASSERT_EQ(test.getProperty3(), 10);
    }
}
