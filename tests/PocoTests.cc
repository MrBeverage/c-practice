/*
 * PocoTests.cc
 *
 *  Experiementing with combinations of std::tuple and some macros to recreate the ease of
 *  class creation and boilerplate elimination present in Java.
 *
 *  Created on: Jan 19, 2015
 *      Author: alexbeverage
 */

#include <memory>
#include <tuple>

#include "gtest/gtest.h"

using namespace std;

namespace Poco {


    TEST(PocoTest, Test1) {

        auto tuple1 = make_tuple(1, 1.0, "asdf");

        auto val0 = get<0>(tuple1);
        auto val1 = get<1>(tuple1);
        auto val2 = get<2>(tuple1);

        cout << val0 << ", " << val1 << ", " << val2 << endl;
    }
}

