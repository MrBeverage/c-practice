# A simple makefile that supports the compilation of any number of sources under src/
# and tests under tests/ and combines them into a single executable called alltests.
#
# Needs a little additional work if some executable under src is meant to be the the
# primary target of the build.  This makefile is ideal for projects that only create
# libraries and a supporting set of tests for validating it. 

# Names
BIN_NAME = main
TEST_NAME = testsuite

# Where to find code.
SRC_DIR = src
INC_DIR = include
TESTS_DIR = tests
GTEST_DIR = libs/gtest
LIBS =

# Build dirs - if you change these you may need to update .gitignore.
BIN_DIR = bin
BUILD_DIR = build
SRC_BUILD_DIR = $(BUILD_DIR)/src
TEST_BUILD_DIR = $(BUILD_DIR)/tests

# Build the list of expected object names.
SOURCE_OBJECTS = $(patsubst $(SRC_DIR)/%.cc,   $(SRC_BUILD_DIR)/%.o,  $(wildcard $(SRC_DIR)/[!$(BIN_NAME)]*.cc))
TEST_OBJECTS   = $(patsubst $(TESTS_DIR)/%.cc, $(TEST_BUILD_DIR)/%.o, $(wildcard $(TESTS_DIR)/*.cc))
MAIN_FILE      = $(SRC_DIR)/$(BIN_NAME).cc

# Flags passed to the preprocessor.
CPPFLAGS += -isystem $(GTEST_DIR)/include -I$(INC_DIR) -I$(BOOST_INCLUDE_DIR) 

# Flags passed to the C++ compiler.
CXXFLAGS += -g -Wall -Wextra -std=c++11

# Add boost support.
BOOST_LIBS = -lboost_regex
LIBS += $(BOOST_LIBS)

# All Google Test headers.
GTEST_HEADERS = $(GTEST_DIR)/include/gtest/*.h \
                $(GTEST_DIR)/include/gtest/internal/*.h


# Main build targets.
all : $(BIN_DIR)/$(BIN_NAME) $(BIN_DIR)/$(TEST_NAME) | $(SRC_BUILD_DIR) $(TEST_BUILD_DIR) $(BIN_DIR)
src : $(BIN_DIR)/$(BIN_NAME) | $(SRC_BUILD_DIR)

$(BIN_DIR) :
	mkdir -p $@
	
clean :
	rm -rf $(BUILD_DIR)
	rm -rf $(BIN_DIR)

# Build gtest if necessary.
GTEST_SRCS_ = $(GTEST_DIR)/src/*.cc $(GTEST_DIR)/src/*.h $(GTEST_HEADERS)

$(TEST_BUILD_DIR)/gtest-all.o : $(GTEST_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) $(CXXFLAGS) -c $(GTEST_DIR)/src/gtest-all.cc -o $@

$(TEST_BUILD_DIR)/gtest_main.o : $(GTEST_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) $(CXXFLAGS) -c $(GTEST_DIR)/src/gtest_main.cc -o $@

$(TEST_BUILD_DIR)/gtest.a : $(TEST_BUILD_DIR)/gtest-all.o
	$(AR) $(ARFLAGS) $@ $^

$(TEST_BUILD_DIR)/gtest_main.a : $(TEST_BUILD_DIR)/gtest-all.o $(TEST_BUILD_DIR)/gtest_main.o
	$(AR) $(ARFLAGS) $@ $^


# Build all sources.
$(SRC_BUILD_DIR)/%.o : $(SRC_DIR)/%.cc | $(SRC_BUILD_DIR)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@
$(SRC_BUILD_DIR) : 
	mkdir -p $@
	
# Build all tests.
$(TEST_BUILD_DIR)/%.o : $(TESTS_DIR)/%.cc $(GTEST_HEADERS) | $(TEST_BUILD_DIR)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@
$(TEST_BUILD_DIR) :
	mkdir -p $@
	
# Merge all tests into a testsuite executable.
$(BIN_DIR)/$(TEST_NAME) : $(SOURCE_OBJECTS) $(TEST_OBJECTS) $(TEST_BUILD_DIR)/gtest_main.a
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(LIBS) -lpthread $^ -o $@

# Build the solution executable.
$(BIN_DIR)/$(BIN_NAME) : $(SOURCE_OBJECTS) $(MAIN_FILE) | $(BIN_DIR)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(LIBS) -lpthread $^ -o $@
	