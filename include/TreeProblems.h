#pragma once

#include <sstream>
#include <queue>

#include "SimpleTreeNode.h"

using namespace SimpleTree;
using namespace std;

namespace TreeProblems {

    template<typename T>
    void preOrderRecursive(Node<T> current, ostringstream & result) {

        if (current == nullptr) {
            return;
        }

        result << *current->getValue();

        preOrderRecursive(current->getLeft(), result);
        preOrderRecursive(current->getRight(), result);
    }

    template<typename T>
    void inOrderRecursive(Node<T> current, ostringstream & result) {

        if (current == nullptr) {
            return;
        }

        inOrderRecursive(current->getLeft(), result);
        result << *current->getValue();
        inOrderRecursive(current->getRight(), result);
    }

    template<typename T>
    void postOrderRecursive(Node<T> current, ostringstream & result) {

        if (current == nullptr) {
            return;
        }

        postOrderRecursive(current->getLeft(), result);
        postOrderRecursive(current->getRight(), result);
        result << *current->getValue();
    }

}
