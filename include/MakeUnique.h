
#pragma once

#include <memory>

//  Add make_unique to C++ 11.  Missed by oversight and will be included in C++ 14.
#ifdef HAS_MOVE_SEMANTICS

namespace std {

template<typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args)
{
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

}

#endif
