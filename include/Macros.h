#pragma once

#include <memory>

/**
 * Defines a member variable, getter, and setter.  Capitalize your name token if you want
 * your getter/setter pair to be well named.
 */
#define PROPERTY(object_name, object_type) \
    private: \
        object_type m_##object_name; \
    public : \
        object_type get##object_name() const { return m_##object_name; } \
        void set##object_name(object_type newValue) { this->m_##object_name = newValue; } \

#define FLUENT_PROPERTY(object_name, object_type) \
    private: \
        object_type m_##object_name; \
    public: \
        object_type get##object_name() const { return m_##object_name; } \
        auto set##object_name(object_type newValue) -> decltype(this) \
        { \
            this->m_##object_name = newValue; \
            return this; \
        } \

#define FLUENT_PROPERTY_SHARED_PTR(object_name, object_type) \
    private: \
        std::shared_ptr<object_type> m_##object_name; \
    public: \
        std::shared_ptr<object_type> get##object_name() const { return m_##object_name; } \
        auto set##object_name(shared_ptr<object_type> newValue) -> decltype(this) \
        { \
            this->m_##object_name = newValue; \
            return this; \
        } \

