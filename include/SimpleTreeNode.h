#pragma once

#include <iostream>
#include <memory>

#include "macros.h"

using namespace std;

//  Idea: bundle data structures in separate namespaces so handy typedefs like Node<T>, Value<T>
//  can be used across multiple objects.  Is this convinience worth it in complex code using many
//  different structures though?
namespace SimpleTree {

template<typename T> class SimpleTreeNode;
template<typename T> using Value = shared_ptr<T>;
template<typename T> using Node  = shared_ptr<SimpleTreeNode<T>>;
template<typename T> using NodePtr = SimpleTreeNode<T> *;

template<typename T> class SimpleTreeNode {

public:
    SimpleTreeNode() :
            SimpleTreeNode(nullptr, nullptr, nullptr, nullptr) {
    }

    SimpleTreeNode(T value) :
            SimpleTreeNode(make_shared<T>(value), nullptr, nullptr, nullptr) {
    }

    SimpleTreeNode(Value<T> value) :
            SimpleTreeNode(value, nullptr, nullptr, nullptr) { }

    SimpleTreeNode(Value<T> value, Node<T> left, Node<T> right, Node<T> head) {
        this->setValue(value);
        this->setLeft(left);
        this->setRight(right);
        this->setHead(head);
    }

    //  Treenode properties.
    FLUENT_PROPERTY_SHARED_PTR(Value, T)
    FLUENT_PROPERTY_SHARED_PTR(Left, SimpleTreeNode<T>)
    FLUENT_PROPERTY_SHARED_PTR(Right, SimpleTreeNode<T>)

    //  A head pointer is included to make test data generation easier.
    //  This is something you would probably not want if making an actual tree class.
    FLUENT_PROPERTY_SHARED_PTR(Head, SimpleTreeNode<T>)
};

template<typename T>
static Node<T> make_node(T value) {
    return make_shared<SimpleTreeNode<T>>(value);
}

template<typename T>
std::ostream & operator<<(std::ostream & stream, const SimpleTreeNode<T> & node) {
    return stream << *(node.getValue());
}

}
